const minute = document.getElementById("minute");
const second = document.getElementById("second");
const list = document.getElementById("list");
const add = document.getElementById("add");
const start = document.getElementById("start");
const reset = document.getElementById("reset");

let arr=[];
let timer=0;
let btnBol=false;
let getTimer = {...JSON.parse(localStorage.getItem("timerKey"))}
console.log(getTimer);

window.addEventListener("load",()=>{
	if (getTimer.min>=0 && getTimer.sec >=0) {
		if(getTimer.min<10) {
			minute.innerText=`0${getTimer.min}`;
		}
		else {
			minute.innerText=getTimer.min;
		}
		if(getTimer.sec<10) {
			second.innerText=`0${getTimer.sec}`;
		}
		else {
			second.innerText=getTimer.sec;
		}
	}
	console.log("Window Load");
})
const setLocal= (value)=>{
	localStorage.setItem("timerKey",JSON.stringify(value))
}
start.addEventListener("click",()=>{
	if(timer == 0) {
		startTimer();
	} else {
		clearInterval(timer);
		timer=0;
	}
})
reset.addEventListener("click",()=>{
	clearInterval(timer);
	getTimer.min=00;
	getTimer.sec=00;
	second.innerHTML="00";
	minute.innerHTML="00";
	localStorage.removeItem("timerKey");
	arr.splice(0,arr.length);
	upDateUI()
})
add.addEventListener("click",()=> {
	let newObj= {min:getTimer.min,sec:getTimer.sec,id:arr.length+1	};
	arr.push(newObj);
	setLocal(newObj);
	upDateUI();
})

const upDateUI =() => {
	let itemsHtml = ""
	arr.forEach((item)=>{
		itemsHtml+=uiAdd(item);
	})
	list.innerHTML = itemsHtml;
}
const removeItem= (id) => {
	const findIndex = arr.findIndex((item)=> item.id === id);
	arr.splice(findIndex,1);
	upDateUI()
}
const uiAdd = (item) => {
	console.log();
	if (item.sec>=0) {
		return `
			<div class="list_child">
				<div>
					<span id="min_sef">${item.id}. ${item.min>10 ? item.min: `0${item.min}`}</span></span> :
					<span id="sec_sef">${item.sec>10 ? item.sec:`0${item.sec}`}</span>
				</div>
				<span onClick="removeItem(${item.id})" id="remove_btn">x</span>
			</div>`
	} else {
		return `
		<div class="list_child">
			<div>
				<span id="min_sef">Empty</span></span>
			</div>
			<span onClick="removeItem(${item.id})" id="remove_btn">x</span>
		</div>`
	}
	
}
const startTimer=()=>{
	if (getTimer.min>=0 && getTimer.sec >=0) {
		timer = setInterval(()=>{
			getTimer.sec++
			if(getTimer.sec>59){
				getTimer.sec=0;
				getTimer.min++
				if(getTimer.min<10) {
					minute.innerText=`0${getTimer.min}`;
				}
				else {
					minute.innerText=getTimer.min;
				}
			} else {
				if(getTimer.sec<10) {
					second.innerText=`0${getTimer.sec}`;
				}
				else {
					second.innerText=getTimer.sec;
				}
			}
		},1000)
	} else {
		getTimer.min=00;
		getTimer.sec=00;
		timer = setInterval(()=>{
			getTimer.sec++
			if(getTimer.sec>59){
				getTimer.sec=0;
				getTimer.min++
				if(getTimer.min<10) {
					minute.innerText=`0${getTimer.min}`;
				}
				else {
					minute.innerText=getTimer.min;
				}
			}
			if(getTimer.sec<10) {
				second.innerText=`0${getTimer.sec}`;
			}
			else {
				second.innerText=getTimer.sec;
			}
		},1000)

	}
	
	console.log("Start",getTimer);
}

